import massive.munit.TestSuite;

import pixelami.css.ParserTest;
import pixelami.css.TokenizerTest;

/**
 * Auto generated Test Suite for MassiveUnit.
 * Refer to munit command line tool for more information (haxelib run munit)
 */

class TestSuite extends massive.munit.TestSuite
{		

	public function new()
	{
		super();

		add(pixelami.css.ParserTest);
		add(pixelami.css.TokenizerTest);
	}
}
