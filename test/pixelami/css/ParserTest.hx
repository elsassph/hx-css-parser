package pixelami.css;

import pixelami.css.CSSParserToken.TokenType;
import massive.munit.Assert;
import pixelami.css.CSSParserToken.TokenType;

class ParserTest {

	var parser:Parser;

	@BeforeClass
	function loadResources() {

	}

	@Before
	function before()
	{
		//parser = new Parser();
	}

	@Test
	function checkEndingTokenMatch()
	{
		var tokenType = Parser.getEndingToken(TokenType.CURLY_OPEN);
		Assert.areEqual(TokenType.CURLY_CLOSE, tokenType);
		Assert.areNotEqual(TokenType.CURLY_OPEN, tokenType);
	}
}
